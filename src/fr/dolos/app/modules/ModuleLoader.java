package fr.dolos.app.modules;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import fr.dolos.app.DolosCore;
import fr.dolos.sdk.events.SystemModulesLoadedEvent;
import fr.dolos.sdk.modules.Module;
import fr.dolos.sdk.modules.ModuleType;
import fr.dolos.sdk.modules.SystemModule;
import fr.dolos.sdk.modules.UserModule;

/**
 * @author Mathias
 */
public class ModuleLoader implements IModuleOpener {

    // Protected for tests purpose
    private final TreeSet<Module> modules = ModuleLoader.getModuleTreeSet();
    private IModuleOpener opener = this;
    private final DolosCore core;

    public ModuleLoader(DolosCore core) {
        this.core = core;
    }

    public void updateModules() {
        for (Module module : modules) {
            module.update();
        }
    }

    public void setOpener(IModuleOpener opener) {
        this.opener = opener;
    }

    /**
     * Instantiate the module TreeSet with a custom comparator allowing module
     * sorting by dependencies
     *
     * @return TreeSet
     */
    public static TreeSet<Module> getModuleTreeSet() {
        // Used to sort modules using their dependencies
        Comparator<Module> comparator = new Comparator<Module>() {
            @Override
            public int compare(Module o1, Module o2) {
                List<String> deps01 = o1.dependencies();
                List<String> deps02 = o2.dependencies();

                if (o1.getName() == o2.getName()) {
                    return (0);
                }
                if (deps01 != null && deps02 != null
                        && deps01.contains(o2.getName())
                        && deps02.contains(o1.getName())) {
                    throw new RuntimeException("Cyclic dependencies beetween modules " + o1.getName() + " and " + o2.getName());
                }

                if (deps01 != null && deps01.contains(o2.getName())) {
                    return (1);
                }
                if (deps02 != null && deps02.contains(o1.getName())) {
                    return (-1);
                }

                if (o2.type() == ModuleType.SYSTEM && o1.type() == ModuleType.USER) {
                    return (1);
                }
                if (o1.type() == ModuleType.SYSTEM && o2.type() == ModuleType.USER) {
                    return (-1);
                }
                return 1;
            }
        };
        return (new TreeSet<Module>(comparator));
    }

    /**
     * Load system and users modules </br>
     * This function will also initialize all modules
     *
     * @param core ICore implementation
     */
    public boolean load() {
        modules.addAll(opener.openModules());
        try {
            this.preloadModules();
            this.loadModules();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Unable to load modules, exiting");
            return (false);
        }
        return (true);
    }

    @Override
    public List<Module> openModules() {
        List<Module> modules = new ArrayList<Module>();
        // Used to fetch only .jar files
        FilenameFilter moduleNameFilter = new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                return (name.endsWith(".jar"));
            }
        };

        // Load modules jars and IModule user implementation
        File modulesFolder = new File("./modules/system");

        if (!modulesFolder.exists()) {
            modulesFolder.mkdirs();
        }
        File jars[] = modulesFolder.listFiles(moduleNameFilter);
        modules.addAll(this.loadJars(jars));

        modulesFolder = new File("./modules");
        jars = modulesFolder.listFiles(moduleNameFilter);
        modules.addAll(this.loadJars(jars));
        return (modules);
    }

    /**
     * Unload all modules
     */
    public boolean unload() {
        boolean ret = true;
        Iterator<Module> it = modules.descendingIterator();
        while (it.hasNext()) {
            Module mod = it.next();
            try {
                mod.unload();
            } catch (Exception e) {
                ret = false;
                e.printStackTrace();
                System.err.println("Error when unloading module " + mod.getName());
            }
            it.remove();
        }
        return (ret);
    }

    /**
     * Find a module by its name
     *
     * @param name Module to find
     * @return Null if not found
     */
    public Module getModule(String name) {
        for (Module module : modules) {
            if (module.getName().equals(name)) {
                return (module);
            }
        }
        return (null);
    }

    /**
     * Check modules dependencies
     *
     * @param module Module to check
     * @throws RuntimeException Exception thrown only if the dependencies are
     * not satisfied
     */
    private void checkDependenciesSatisfied(Module module) throws RuntimeException {
        List<String> dependencies = module.dependencies();
        if (dependencies == null) {
            return;
        }
        for (String dep : dependencies) {
            if (this.getModule(dep) == null) {
                throw new RuntimeException("Dependency " + dep + " for " + module.getName() + " is missing or not loaded");
            }
        }
    }

    /**
     * Preload modules
     *
     * @throws Exception
     */
    protected void preloadModules() throws Exception {
        Iterator<Module> it = modules.iterator();
        while (it.hasNext()) {
            Module module = it.next();
            this.checkDependenciesSatisfied(module);
            if (!module.preload()) {
                it.remove();
            }
        }
    }

    /**
     * Load modules
     *
     * @throws Exception
     */
    protected void loadModules() throws Exception {
        Iterator<Module> it = modules.iterator();
        boolean eventThrown = false;
        while (it.hasNext()) {
            Module module = it.next();
            System.out.println(String.format("Loading module %s", module.getName()));
            this.checkDependenciesSatisfied(module);
            if (module.type() == ModuleType.SYSTEM) {
                if (!((SystemModule) module).load(core, core.getSystemManager())) {
                    throw new RuntimeException("Unable to load module " + module.getName());
                }
            } else {
                if (!eventThrown) {
                    core.getEventManager().post(new SystemModulesLoadedEvent());
                    eventThrown = true;
                }
                if (!((UserModule) module).load(core)) {
                    throw new RuntimeException("Unable to load module " + module.getName());
                }
            }
            System.out.println(String.format("Module %s loaded", module.getName()));
        }
        if (!eventThrown) // Security check, event may not be thrown during the loop if there are no user modules
        {
            core.getEventManager().post(new SystemModulesLoadedEvent());
        }
    }

    /**
     * Load modules inside jars
     *
     * @param files Jars files to load
     * @return TreeSet of modules
     */
    private TreeSet<Module> loadJars(File files[]) {
        TreeSet<Module> modules = ModuleLoader.getModuleTreeSet();
        for (File f : files) {
            try {
                Module module = this.loadModuleFromjar(f);
                if (module == null) {
                    throw new ClassNotFoundException("IModule implementation not found");
                }
                modules.add(module);
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IOException e) {
                e.printStackTrace();
                System.err.println("Unable to load module " + f.getAbsolutePath());
            }
        }
        return (modules);
    }

    /**
     * Load module instance from a jar file
     *
     * @param f The jar file to load
     * @return IModule contained, null if no modules found
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    @SuppressWarnings("deprecation")
	private Module loadModuleFromjar(File f) throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        JarFile jar = new JarFile(f);
        Enumeration<JarEntry> entries = jar.entries();
        Module module = null;

        URL[] urls = {new URL("jar:file:" + f.getAbsolutePath() + "!/")};
        URLClassLoader cl = URLClassLoader.newInstance(urls);
        while (entries.hasMoreElements()) {
            JarEntry je = entries.nextElement();
            if (je.isDirectory() || !je.getName().endsWith(".class")) {
                continue;
            }
            // -6 because of .class
            String className = je.getName().substring(0, je.getName().length() - 6);
            className = className.replace('/', '.');
            Class<?> c = cl.loadClass(className);
            if (module == null && Module.class.isAssignableFrom(c)) {
                module = (Module) c.newInstance();
                break;
            }
        }
        jar.close();
        return (module);
    }
}
