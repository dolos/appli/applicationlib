package fr.dolos.app.modules;

import java.util.List;

import fr.dolos.sdk.modules.Module;

public interface IModuleOpener {

    public List<Module> openModules();
}
