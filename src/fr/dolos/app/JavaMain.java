package fr.dolos.app;

import fr.dolos.sdk.UIManager;
import javafx.scene.control.Tab;
import javafx.scene.layout.Pane;

public class JavaMain {

    private static final String MSG_START = "Starting", MSG_INIT = "Initializing",
            MSG_AWAITING = "Waiting for commands", MSG_UNLOAD = "Unloading", MSG_END = "Stopping";

    public static void main(String[] args) {
        System.out.println(MSG_START);
        DolosCore core = new DolosCore(new UIManager() {
			
			@Override
			public Tab createTab(String moduleName) {
				return new Tab();
			}
			
			@Override
			public void bindWidthAndEight(Pane pane) {
			}
		});
        System.out.println(MSG_INIT);
        core.init();
        System.out.println(MSG_AWAITING);
        core.loop();
        System.out.println(MSG_UNLOAD);
        core.release();
        System.out.println(MSG_END);
    }

}
