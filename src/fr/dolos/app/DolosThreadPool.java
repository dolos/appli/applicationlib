package fr.dolos.app;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import fr.dolos.sdk.ThreadPool;

public class DolosThreadPool implements ThreadPool {

    private final static int CORE_THREAD = 3;

    private final ScheduledExecutorService pool;
    private final Queue<Runnable> mainThreadTasks;

    public DolosThreadPool() {
        pool = Executors.newScheduledThreadPool(CORE_THREAD);
        this.mainThreadTasks = new ArrayBlockingQueue<>(300);
    }

    public void shutdown() {
        pool.shutdownNow();
    }

    public void pullTasks() {
    	ArrayList<Runnable> copy = new ArrayList<Runnable>();
        while (!mainThreadTasks.isEmpty())
            copy.add(mainThreadTasks.poll());
        copy.forEach(item -> item.run());
    }

    @Override
    public <T> Future<T> runAsync(Callable<T> callable) {
        return pool.submit(callable);
    }

    @Override
    public <T> Future<T> runOnDedicated(Callable<T> callable) {
        return pool.submit(callable);
    }

    @Override
    public <T> ScheduledFuture<T> runDelayed(Callable<T> callable, long delay) {
        return pool.schedule(callable, delay, TimeUnit.MILLISECONDS);
    }

    @Override
    public ScheduledFuture<?> runTimed(Runnable runnable, long delay, long timer) {
        return pool.scheduleWithFixedDelay(runnable, delay, timer, TimeUnit.MILLISECONDS);
    }

    @Override
    public void runOnCoreThread(Runnable runnable) {
        mainThreadTasks.add(runnable);
    }
}
