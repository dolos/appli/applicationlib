package fr.dolos.app;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;

import fr.dolos.sdk.events.EventHandler;
import fr.dolos.sdk.events.EventManager;

/**
 * EventManager allowing modules to communicate between them and with the Core
 *
 * @author Mathias
 *
 */
public class DolosEventManager implements EventManager {

    /**
     * Simple container used for easy EH managing
     *
     * @author Mathias
     *
     */
    private class EventContainer {

        Object eh;
        Method method;
        EventHandler.EventPriority priority;

        EventContainer(Object eh, Method method, EventHandler.EventPriority priority) {
            this.eh = eh;
            this.method = method;
            this.priority = priority;
        }

        <T> boolean handleEvent(Class<T> eventClass, EventHandler.EventPriority priority) {
            return (this.priority == priority && method.getParameterTypes()[0].equals(eventClass));
        }

        <T> void invoke(T event) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            method.invoke(eh, event);
        }
    }

    private final ArrayList<EventContainer> handlers = new ArrayList<EventContainer>();

    /**
     * Send an event through the EventManager
     */
    @Override
    public <T> void post(T event) {
        for (EventHandler.EventPriority p : EventHandler.EventPriority.values()) {
            for (EventContainer container : handlers) {
                if (!container.handleEvent(event.getClass(), p)) {
                    continue;
                }
                try {
                    container.invoke(event);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    System.err.println("Unable to post event");
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Register a new event handler
     *
     * @return integer idx of the event handler
     */
    @Override
    public void registerEventHandler(Object o) {
        Class<?> klass = o.getClass();

        while (klass != Object.class) {
            Method[] klassMeth = klass.getDeclaredMethods();
            for (Method method : klassMeth) {
                if (method.isAnnotationPresent(EventHandler.class) && method.getParameterCount() == 1) {
                    EventHandler anno = method.getAnnotation(EventHandler.class);
                    handlers.add(new EventContainer(o, method, anno.priority()));
                }
            }
            klass = klass.getSuperclass();
        }
    }

    /**
     * Remove a event handler
     */
    @Override
    public void removeEventHandler(Object eh) {
        Iterator<EventContainer> it = handlers.iterator();
        while (it.hasNext()) {
            EventContainer container = it.next();
            if (container.eh == eh) {
                it.remove();
            }
        }
    }
}
