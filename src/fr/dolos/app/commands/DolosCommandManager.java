package fr.dolos.app.commands;

import fr.dolos.sdk.commands.CommandHandler;
import fr.dolos.sdk.commands.CommandManager;
import fr.dolos.sdk.exceptions.CommandNotFoundException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.regex.PatternSyntaxException;

/**
 * Utility to handle commands from the command line
 * Can also be used to trigger commands from a module
 * @author Mathias Hyrondelle
 */
public class DolosCommandManager extends CommandManager{

    private final HashMap<String, CommandHandler> handlers = new HashMap<>();

    @Override
    public void registerHandler(String label, CommandHandler handler) {
        synchronized (handlers) {
            handlers.put(label, handler);
        }
    }

    @Override
    public void readCommand() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            String line = reader.readLine();
            if (line == null) {
                return;
            }
            String[] args = line.split(" ");
            this.executeCommand(args[0], args);
        } catch (IOException | PatternSyntaxException | CommandNotFoundException e) {
            e.printStackTrace(System.err);
        }
    }

    @Override
    public void executeCommand(String command, String[] args) throws CommandNotFoundException
    {
        CommandHandler h;
        synchronized (handlers) {
            h = handlers.get(args[0]);
        }
        if (h == null)
            throw new CommandNotFoundException(command);
        h.onCommand(args[0], args);
    }
}
