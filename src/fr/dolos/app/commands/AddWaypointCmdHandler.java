package fr.dolos.app.commands;

import java.io.IOException;
import java.util.List;

import fr.dolos.sdk.Core;
import fr.dolos.sdk.commands.CommandHandler;
import fr.dolos.sdk.models.Drone;
import fr.dolos.sdk.network.Packet;

/**
 * Handle adding waypoint to the drone
 * @author Mathias
 */
public class AddWaypointCmdHandler implements CommandHandler {
    
    private static final String WAYPOINT_PACKET_NAME = "DOLOS_WAYPOINT_PACKET";

    public static class WaypointPacket implements Packet
    {
        public final double x, y, z;
        
        public WaypointPacket(final double x, final double y, final double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        
        @Override
        public String getName() {
            return WAYPOINT_PACKET_NAME;
        }

        @Override
        public String serialize() {
            return String.format("%f:%f:%f", x, y, z);
        }
    }
    
    private final Core core;
    
    /**
     *
     * @param core Core to use
     */
    public AddWaypointCmdHandler(final Core core)
    {
        this.core = core;
    }
    
    /**
     * Handle AddWaypoint command
     * @param label
     * @param args  Args[1] = drone ip
     *              Args[2] = coord x
     *              Args[3] = coord y
     *              Args[4] = coord z
     */
    @Override
    public void onCommand(String label, String[] args) {
        if (args.length != 5)
        {
            System.err.println(String.format("Invalid usage: %s ip x y z", label));
            return;
        }
        
        List<? extends Drone> clients = core.getDroneList();
        // Check if the list contain a drone with the requested ip
        if (!clients.stream().anyMatch((client) -> (client.getIp().equals(args[1]))))
        {
            System.err.println(String.format("%s: drone %s not found", label, args[1]));
            return;
        }
        
        final double x = Double.parseDouble(args[2]);
        final double y = Double.parseDouble(args[3]);
        final double z = Double.parseDouble(args[4]);
        
        // Fetch the drone with the requested ip and send it the wp information
        clients.stream()
                .filter((client) -> (client.getIp().equals(args[1])))
                .forEach((client) -> {
                    try {
                        System.out.println("Sending waypoint to " + args[1]);
                        client.getConnection().send(new WaypointPacket(x, y, z));
                    } catch (IOException e)
                    {
                        e.printStackTrace(System.err);
                        System.err.println("Unable to send Waypoint data");
                    }
               });
    }
}