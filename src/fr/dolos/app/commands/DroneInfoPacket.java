package fr.dolos.app.commands;

import java.util.Scanner;

import fr.dolos.sdk.network.Packet;

public class DroneInfoPacket implements Packet
{
    public final double lan, lon, alt;
    
    public DroneInfoPacket(String packet, String data) {
        Scanner sc = new Scanner(data);
        lan = Double.parseDouble(sc.next());
        lon = Double.parseDouble(sc.next());
        alt = Double.parseDouble(sc.next());
        sc.close();
    }

	@Override
    public String getName() {
        return "DRONE_INFO_PACKET";
    }

    @Override
    public String serialize() {
        return "";
    }
}
