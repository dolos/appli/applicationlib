package fr.dolos.app;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;

import fr.dolos.app.commands.AddWaypointCmdHandler;
import fr.dolos.app.commands.DolosCommandManager;
import fr.dolos.app.commands.DroneInfoPacket;
import fr.dolos.app.modules.ModuleLoader;
import fr.dolos.sdk.Core;
import fr.dolos.sdk.ThreadPool;
import fr.dolos.sdk.UIManager;
import fr.dolos.sdk.commands.CommandHandler;
import fr.dolos.sdk.commands.CommandManager;
import fr.dolos.sdk.commands.DolosCommand;
import fr.dolos.sdk.events.ClientConnectedEvent;
import fr.dolos.sdk.events.ClientDisconnectedEvent;
import fr.dolos.sdk.events.EventHandler;
import fr.dolos.sdk.events.EventManager;
import fr.dolos.sdk.models.Drone;
import fr.dolos.sdk.models.Position;
import fr.dolos.sdk.modules.SystemManager;
import fr.dolos.sdk.network.NetworkClient;
import fr.dolos.sdk.network.NetworkManager;
import fr.dolos.sdk.network.Packet;
import fr.dolos.sdk.network.PacketDeserializer;
import fr.dolos.sdk.network.PacketHandler;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;


/**
 * @author Mathias Hyrondelle
 */
public class DolosCore extends Core implements CommandHandler {
    
    private final ModuleLoader moduleLoader;
    private final DolosEventManager eventManager;
    private final DolosNetworkManager networkManager;
    private final DolosThreadPool threadPool;
    private final SystemManager systemManager;
    private final CommandManager cmdManager;
    private final UIManager uiManager;
    private boolean initialized = false;
    private volatile boolean loop = false;
    private final ObservableList<Drone> listDrone;
    
    public DolosCore(final UIManager uiManager) {
    	this.uiManager = uiManager;
    	this.listDrone = FXCollections.observableArrayList();
        this.cmdManager = new DolosCommandManager();
        this.threadPool = new DolosThreadPool();
        this.eventManager = new DolosEventManager();
        this.systemManager = new DolosSystemManager();
        this.networkManager = new DolosNetworkManager(this);
        this.moduleLoader = new ModuleLoader(this);
    }

    static class DroneInfoDeserializer implements PacketDeserializer {
	    public Packet deserialize(String packet, String data) {
	    	return new DroneInfoPacket(packet, data);
	    }
	}
    
    public boolean init() {
        eventManager.registerEventHandler(this);
        networkManager.registerReceiver(this);
        cmdManager.registerHandler(DolosCommand.CONNECT, this);
        cmdManager.registerHandler(DolosCommand.STOP, this);
        cmdManager.registerHandler(DolosCommand.ADD_WAYPOINT, new AddWaypointCmdHandler(this));
        networkManager.registerDeserializer("DRONE_INFO", new DroneInfoDeserializer());
        cmdManager.registerHandler("drones", this);
        initialized = moduleLoader.load();
        return initialized;
    }

    public void release() {
        if (!initialized) {
            return;
        }
        moduleLoader.unload();
        threadPool.shutdown();
    }

    /**
     * Loop used for debug purpose
     */
    public void loop() {
        loop = true;
        ScheduledFuture<?> readerThread = threadPool.runTimed(() -> {
            if (loop) {
                cmdManager.readCommand();
            }
        }, 100, 1);
        while (loop) {
            this.update();
        }
        readerThread.cancel(true);
    }
    
    public void stopLoop()
    {
    	loop = false;
    }

    /**
     * Update everything
     */
    public void update() {
        try {
            moduleLoader.updateModules();
            threadPool.pullTasks();
        } catch (Exception e) // handle any exception to avoid crash
        {
            e.printStackTrace(System.err);
        }
    }

    public SystemManager getSystemManager() {
        return systemManager;
    }

    public ModuleLoader getModuleLoader() {
        return (moduleLoader);
    }
    
    @Override
    public CommandManager getCommandManager() {
        return (cmdManager);
    }

    @Override
    public void runOnCoreThread(final Runnable runnable) {
        threadPool.runOnCoreThread(runnable);
    }

    @Override
    public EventManager getEventManager() {
        return eventManager;
    }

    @Override
    public NetworkManager getNetworkManager() {
        return networkManager;
    }

    @Override
    public ThreadPool getScheduler() {
        return threadPool;
    }
    
    @Override
    public ObservableList<Drone> getDroneList() {
    	return this.listDrone;
    }
    
    /**
     * Handle commands CONNECT and STOP
     * @param label Command label
     * @param args Command arguments
     */
    @Override
    public void onCommand(String label, String[] args) {      
        if (label.equals(DolosCommand.CONNECT.label)) {
            if (args.length != 3) {
                System.err.println(String.format("%s: 3 arguments needed : connect ip port", label));
                return;
            }
            String ip = args[1];
            int port = Integer.parseInt(args[2]);
            Future<NetworkClient> future = networkManager.connectTo(ip, port, Core.getInstance().getDroneList().size());
            if (future == null)
            {
                System.err.println("Unable to initialize connection in network manager");
                return;
            }
            System.out.println("Waiting for connection");
            try {
                NetworkClient client = future.get();
                if (client == null) {
                    System.out.println("Connection failed");
                    return;
                }
                System.out.println("Connection succeeded");
                System.out.println(String.format("Client connected from: %s", client.getIp()));
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace(System.err);
            }
        }
        else if (label.equals(DolosCommand.STOP.label))
            loop = false;
        else if (label.equals("drones"))
        {
            System.out.println("Current connected clients:");
            listDrone.stream()
                    .forEach((client) -> { System.out.println(client.getIp()); });
        }
    }
    
    @EventHandler(priority = EventHandler.EventPriority.HIGHEST)
    public void onClientConnected(ClientConnectedEvent event)
    {
    	if (Platform.isFxApplicationThread())
    		listDrone.add(event.getClient());
    	else
    		Platform.runLater(() -> listDrone.add(event.getClient()));
    }
    
    @EventHandler(priority = EventHandler.EventPriority.HIGHEST)
    public void onClientDisconnected(ClientDisconnectedEvent event)
    {
    	if (Platform.isFxApplicationThread())
    		listDrone.remove(event.getClient());
    	else
    		Platform.runLater(() -> listDrone.remove(event.getClient()));
    }

    @PacketHandler
    public void onReceiveDroneInfo(DroneInfoPacket packet, NetworkClient client)
    {
    	List<? extends Drone> list = getDroneList();
    	for (Drone drone : list) {
    		if (drone.getIp().equals(client.getIp()))
    			drone.setPosition(new Position(packet.lan, packet.lon, packet.alt));
    	}
    }

    @Override
    public UIManager getUIManager() {
    	return (uiManager);
    }
}
