package fr.dolos.app;

import fr.dolos.sdk.modules.SystemManager;
import fr.dolos.sdk.network.ConnectionInstantiater;

/**
 * System module manager
 * Allow users to set system modules
 * @author Mathias Hyrondelle
 */
public class DolosSystemManager implements SystemManager {

    private ConnectionInstantiater instantiater = null;

    @Override
    public void setConnectionInstantiater(ConnectionInstantiater instantiater) {
        this.instantiater = instantiater;
    }

    @Override
    public ConnectionInstantiater getConnectionInstantiater() {
        return instantiater;
    }
}
