package fr.dolos.app;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import fr.dolos.sdk.ThreadPool;
import fr.dolos.sdk.events.ClientConnectedEvent;
import fr.dolos.sdk.events.ClientDisconnectedEvent;
import fr.dolos.sdk.events.EventHandler;
import fr.dolos.sdk.events.EventManager;
import fr.dolos.sdk.events.SystemModulesLoadedEvent;
import fr.dolos.sdk.models.Drone;
import fr.dolos.sdk.modules.SystemManager;
import fr.dolos.sdk.network.ConnectionInstantiater;
import fr.dolos.sdk.network.ConnectionListener;
import fr.dolos.sdk.network.Filter;
import fr.dolos.sdk.network.NetworkClient;
import fr.dolos.sdk.network.NetworkManager;
import fr.dolos.sdk.network.Packet;
import fr.dolos.sdk.network.PacketDeserializer;
import fr.dolos.sdk.network.PacketHandler;
import fr.dolos.sdk.network.PacketInstantiater;

/**
 * Network Manager handling data sending accross network
 * @author Mathias Hyrondelle
 */
public class DolosNetworkManager implements NetworkManager, PacketInstantiater, Filter, ConnectionListener {

    /**
     * Simple container used for easy EH managing
     *
     * @author Mathias
     *
     */
    private class NetworkContainer {

        Object eh;
        Method method;
        PacketHandler.HandlerPriority priority;

        NetworkContainer(Object eh, Method method, PacketHandler.HandlerPriority priority) {
            this.eh = eh;
            this.method = method;
            this.priority = priority;
        }

        <T> boolean handleEvent(Class<T> eventClass, PacketHandler.HandlerPriority priority) {
            return (this.priority == priority && method.getParameterTypes()[0].equals(eventClass));
        }

        <T> void invoke(T event, NetworkClient client) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            method.invoke(eh, event, client);
        }
    }

    private final ArrayList<NetworkContainer> containers = new ArrayList<>();
    private final ArrayList<Filter> filters = new ArrayList<>();
    private final HashMap<String, PacketDeserializer> deserializers = new HashMap<>();
    private final ThreadPool pool;
    private final EventManager eventManager;
    private final SystemManager manager;
    private ConnectionInstantiater connectionInstantiater;

    public DolosNetworkManager(ThreadPool pool, EventManager eventManager, SystemManager manager) {
        this.pool = pool;
        this.eventManager = eventManager;
        this.manager = manager;
        eventManager.registerEventHandler(this);
    }

    public DolosNetworkManager(DolosCore core) {
        pool = core.getScheduler();
        eventManager = core.getEventManager();
        manager = core.getSystemManager();
        eventManager.registerEventHandler(this);
    }

    @EventHandler
    public void onSysLoaded(SystemModulesLoadedEvent event) {
        this.connectionInstantiater = manager.getConnectionInstantiater();
        if (connectionInstantiater == null) {
            System.err.println("Error: no network module found, network won't be available");
            return;
        }
        connectionInstantiater.setConnectionListener(this);
        connectionInstantiater.setFilterApplicative(this);
        connectionInstantiater.setPacketInstantiater(this);
        pool.runOnCoreThread(() -> {
            eventManager.removeEventHandler(DolosNetworkManager.this);
        });
    }

    @Override
    public Future<NetworkClient> connectTo(String ip, int port, int id) {
        if (connectionInstantiater == null)
        {
        	CompletableFuture<NetworkClient> future = new CompletableFuture<>();
        	future.complete(null);
            return (future);
        }
        return connectionInstantiater.connectTo(ip, port, id);
    }

    @Override
    public void addFilter(Filter filter) {
        synchronized (filters) {
            if (!filters.contains(filter)) {
                filters.add(filter);
            }
        }
    }

    @Override
    public void removeFilter(Filter filter) {
        synchronized (filters) {
            if (filters.contains(filter)) {
                filters.remove(filter);
            }
        }
    }

    @Override
    public void registerDeserializer(String packetName, PacketDeserializer deserializer) {
        synchronized (deserializers) {
            if (!deserializers.containsKey(packetName)) {
                deserializers.put(packetName, deserializer);
            }
        }
    }

    @Override
    public Packet instantiate(String packet, String data) {
        synchronized (deserializers) {
            PacketDeserializer deser = deserializers.get(packet);
            if (deser == null) {
                System.err.println("NetworkManager: No deserializer found for packet " + packet);
                return (null);
            }
            return (deser.deserialize(packet, data));
        }
    }

    @Override
    public <T extends Packet> void onPacketSending(T packet) {
        synchronized (filters) {
            filters.forEach((f) -> {
                f.onPacketSending(packet);
            });
        }
    }

    @Override
    public void onPacketReceiving(Packet packet) {
        synchronized (filters) {
            filters.forEach((f) -> {
                f.onPacketReceiving(packet);
            });
        }
    }

    @Override
    public String onDataSending(String data) {
        synchronized (filters) {
            for (Filter f : filters) {
                data = f.onDataSending(data);
            }
        }
        return (data);
    }

    @Override
    public String onDataReceiving(String data) {
        synchronized (filters) {
            for (Filter f : filters) {
                data = f.onDataReceiving(data);
            }
        }
        return (data);
    }

    @Override
    public void onClientReady(Drone client) {
        eventManager.post(new ClientConnectedEvent(client));
    }

    @Override
    public void onClientDisconnected(Drone client) {
        eventManager.post(new ClientDisconnectedEvent(client));
    }

    @Override
    public void onPacketReady(Packet packet, NetworkClient client) {
        for (PacketHandler.HandlerPriority p : PacketHandler.HandlerPriority.values()) {
            // Filter containers to get only those wich handle the packet
            containers.stream().filter((container) -> (container.handleEvent(packet.getClass(), p)))
                    // For each container that handle this packet, invoke it
                    .forEachOrdered((container) -> {
                try {
                    container.invoke(packet, client);
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    System.err.println("Unable to post packet event");
                    e.printStackTrace(System.err);
                }
            });
        }
    }

    @Override
    public void registerReceiver(Object o) {
        Class<?> klass = o.getClass();

        while (klass != Object.class) {
            Method[] klassMeth = klass.getDeclaredMethods();
            for (Method method : klassMeth) {
                if (method.isAnnotationPresent(PacketHandler.class)) {
                    if (method.getParameterCount() != 2) {
                        System.err.println("DolosNetworkManager: Packet Handler must handle 2 arguments : packet type and NetworkClient");
                        continue;
                    }
                    if (method.getParameterTypes()[1] != NetworkClient.class) {
                        throw new RuntimeException("DolosNetworkManager: Packet Handler must handle 2 arguments : packet type and NetworkClient");
                    }
                    PacketHandler anno = method.getAnnotation(PacketHandler.class);
                    containers.add(new NetworkContainer(o, method, anno.priority()));
                }
            }
            klass = klass.getSuperclass();
        }
    }

    @Override
    public void removeReceiver(Object o) {
        Iterator<NetworkContainer> it = containers.iterator();
        while (it.hasNext()) {
            NetworkContainer container = it.next();
            if (container.eh == o) {
                it.remove();
            }
        }
    }
}
