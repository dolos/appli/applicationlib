module applicationlib {
	requires javafx.base;
	exports fr.dolos.app;
	exports fr.dolos.app.modules;
	exports fr.dolos.app.commands;

	requires transitive applicationsdk;
}